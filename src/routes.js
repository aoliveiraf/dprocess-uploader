const routes = require('express').Router()
const multer = require('multer');
const multerConfig = require('./config/multer')
const db = require('./database')

routes.post('/upload',multer(multerConfig).single('file0'),async (req,res)=>{
    //console.log(req.file);
    const { originalname: name, size, key, location: url = "" } = req.file;
    console.log(name);
    console.log(size);
    console.log(key)
    console.log(url);
    let keyDocument = req.query.keyDocument;

  //  let path=req.file.url;
    let cursor = await db.query(`FOR d IN document FILTER d._key ==@keyDocument
              UPDATE d._key WITH {path:@path} IN document let new = NEW return new`,
    {keyDocument:keyDocument,path:url});

    let result = await cursor.all()
 //   console.log(result);
    
    res.status(200).send(result);
  /*  .then((cursor)=>{
        cursor.all()
        .then((result)=>{
            res.status(200).send(result)
        }).catch((error)=>{
            res.status(500).send(`Router - /upload failed: ${error}`);
        })
    }).catch((error)=>{
        res.status(500).send(`Router - /upload failed: ${error}`);

    })
    */
   // return res.json({hello:"world"})
});


module.exports = routes;